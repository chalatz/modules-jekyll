---
---
$(document).ready(function() {

    function handle_slider(){

        var accordion_slide_speed = "{{ site.accordion_slide_speed }}" * 1;
    
        $('.accordion-header').on('click', function(){

            var $this = $(this);

            $this.toggleClass('accordion-item-active');

            $this.next('.accordion-content').slideToggle(accordion_slide_speed);

        });

        $('.close-accordion-item').on('click', function(){

            var $this = $(this),
                accordion_content = $this.parent('.accordion-content'),
                accordion_header = accordion_content.siblings('.accordion-header');

            accordion_content.slideUp(accordion_slide_speed);

            accordion_header.removeClass('accordion-item-active');

        });

    }

    function handle_rotator(){

        var text_item = $('.text-item'),
            text_height = text_item.innerHeight(),
            text_items = $('#text-items'),
            rotator_intro_text = $('.rotator-intro-text');

        var sliding_duration = "{{ site.data.rotator.sliding_duration }}" * 1,
            in_view_duration = "{{ site.data.rotator.in_view_duration }}" * 1,
            text_items_length = "{{ site.data.rotator.items | size }}" * 1,
            fade_distance = "{{ site.data.rotator.last_item_intro.fade_distance | size }}" * 1;

        text_items.height(text_height);

        text_item.removeClass('invisible');

        function move_it(){
            text_items.animate({
                bottom: "+=" + text_height
            }, sliding_duration);
        }

        function fade_it_out(el){
            el.animate({
                bottom: "+=" + fade_distance,
                opacity: 0
            }, sliding_duration/2);
        }        

        function fade_it_in(el){
            el.animate({
                bottom: 0,
                opacity: 1
            }, sliding_duration/2);
        }        

        var last_index = text_items_length - 1;

        if($(window).width() > 500){
            text_item.each(function(index){

                var times = index + 1,
                    $this = $(this);

                if(index < last_index - 1){
                    setTimeout(function(){
                        move_it();
                    }, in_view_duration * times);
                }

                if(index == last_index - 1){
                    setTimeout(function(){
                        move_it();
                        fade_it_out(rotator_intro_text);
                        setTimeout(function(){
                            rotator_intro_text.html("{{ site.data.rotator.last_item_intro.text }}");
                            fade_it_in(rotator_intro_text);
                        },sliding_duration/2);
                    }, in_view_duration * times);
                }

            });
        } else {
            $('.text-item-last').siblings().hide();
        }

    }

    handle_slider();

    handle_rotator();

});