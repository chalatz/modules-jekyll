# Accordion

The accordion items are pulled from a Jekyll collection. They can be found inside the `_accordion` folder.

## Settings

### Images folder

The folder where the accordion images get pulled from, is defined in the `_config.yml` file in this block of code:

```yml
defaults:
  - scope:
      path: ""
      type: accordion
    values:
      accordion_images_folder: '/assets/images/accordion/'
```
The `accordion_images_folder` field, is the one in question.

### Sliding speed

The speed in which the panels will be sliding, is defined in the `_config.yml` file, through the `accordion_slide_speed` field.

Its values are set in milliseconds (1000 milliseconds = 1 second).

## Fields

In each markdown file in the `_accordion` folder, the following fields are used:

- **order**: This is used to sort the panels in the accordion.
- **title**: The title of the panel.
- **image**: The name of the image file that is inside the set images folder.
- **icon**: This the name for the font-awsome icon.
- **text**, **link_text**, **link_url**: Fields used for the intro part of the panel's body

## Writing content

The content for the panels can be written in markdown. If necessary, HTML can be used as well.

## Markup

The `accordion.html` file spits out the accordion markup code.

## Styling

The `_sass/accordion.scss` is used to style the accordion.

## Dependencies

- **jQuery**: Included in the `_includes/footer.html` file.
- **scripts.js**: Included in the `_includes/footer.html` file.
- **Font awsome**: The script is included inside the `<head>` tag, in the `_includes/head.html`

# Text Rotator

## Configuration

The text rotator is configured via the `_data/rotator.yml` file. The fields have comments and their names are quite self explanatory.

## Markup

The `rotator.html` file spits out the rotator markup code.

## Styling

The `_sass/rotator.scss` is used to style the accordion.
