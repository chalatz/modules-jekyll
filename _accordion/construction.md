---
order: 2
title: Contstruction
image: construction.jpg
icon: wrench
intro:
    text: 'Navigant is recognized globally as the provider of choice on construction disputes, as well as for being the source for sophisticated advisory services on the world’s most complex construction projects.'
    link_text: 'Learn more about our Construction capabilities.'
    link_url: '#'
---

**CONSTRUCTION**

## Providing Valuable Quantum Advice

Our client, a joint venture of four construction contractors, won the contract to develop the Dubai Metro - the world’s longest automated driverless metro system, running around 74KM and incorporating 47 stations.

## Challenge

Navigant was appointed as expert advisor to consider the valuation of disputed change orders and entitlement to additional payment arising from prolongation disruption and acceleration. Contentious issues related to the valuation of significant variations, as well as complex entitlement issues including delay, disruption and acceleration.

## Solution

Our team of quantum experts provided a robust valuation and detailed risk analysis in a very short timeframe.  

## Impact
It enabled our client to arrive at an early settlement and avoid a very costly arbitration.  Today, this is still one of the largest disputes by value in the Middle East. 