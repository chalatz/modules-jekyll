---
order: 1
title: Financial Services
image: financial-services.jpg
icon: building
intro:
    text: 'Navigant collaborates with banking, insurance and investment management clients to help them reach operational effectiveness and mitigate compliance risks.'
    link_text: 'Learn more about our Financial Services capabilities.'
    link_url: '#'
---

**FINANCIAL SERVICES**

### Banking Consumer Finance

### Challenge

For a top 5 non-bank mortgage servicer, Navigant assessed complaint handling and resolution processes for complaints that were received by the enterprise call center.

### Solution

Navigant assessed complaint handling and resolution processes for complaints related to: default management; loan modifications; bankruptcy or foreclosure; written complaints related to servicing; third party inquiries including foreclosure and bankruptcy attorneys and corporate complaints from regulators & lawmakers. Additionally, Navigant assessed the quality control and compliance monitoring processes, organizational structure, governance processes, complaint measurement and reporting, complaint handling policies, procedures & business processes, employee training and skills and support systems. We created an end state design for customer complaint handling, for both the residential loan servicing business unit and the parent bank.

### Impact

Our recommendations included short term fixes as well as long term solutions for a world class customer experience solution. 